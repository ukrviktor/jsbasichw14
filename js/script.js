// Завдання №1
const styleBtnContainer = document.querySelector('.change-style-btn');
const defaultStyleBtn = document.querySelector('[data-style-color=default]');
const blueStyleBtn = document.querySelector('[data-style-color=blue]');
const nameKeyForStorage = 'nameStyle';
const nameStyleActive = 'active';

defaultStyleBtn.addEventListener('click', (e) => {
  clearActiveBtn();
  setActiveBtn(e.target);
  clearStyle();
  setStyleFromStorage(e.target.dataset.styleColor);
});

blueStyleBtn.addEventListener('click', (e) => {
  {
    clearActiveBtn();
    setActiveBtn(e.target);
    let userSetStyle = e.target.dataset.styleColor;
    setStyle(blue);
    setStyleFromStorage(e.target.dataset.styleColor);
  }
});

const blue = {
  '.header-wrap .header-title': {
    color: '#423DFF',
  },
  '.header-wrap .header-subtitle': {
    color: '#423DFF',
  },
'.header-wrap .header-contacts': {
  color: '#423DFF',
},
  '.main-wrap .main-wrap-item .main-subtitle': {
  color: '#423DFF',
  fontSize: '20px',
},
'.item-block': {
  background: '#423DFF',
},
'.img-wrap': {
  borderImage: 'linear-gradient(-145grad, #423DFF, #F67503) 2',
},
'.footer-text': {
  color: '#423DFF',
  fontSize: '16px',
},
};

const red = {};

const defaultStyle = [
'.header-wrap .header-contacts',
'.header-wrap .header-title',
'.header-wrap .header-subtitle',
'.main-wrap .main-wrap-item .main-subtitle',
'.item-block',
'.img-wrap',
'.footer-text',
];


let nameStyle = getStyleFromStorage();
if (!nameStyle) {
  setStyleFromStorage('default');
} else {
  switch (nameStyle) {
    case 'blue':
      blueStyleBtn.click();
      break;
    case 'red':
      break;
    default:
      defaultStyleBtn.click();
  }
}


function setActiveBtn(item) {
  item.classList.add(nameStyleActive);
}

function clearActiveBtn() {
  [...styleBtnContainer.children].forEach((btn) => {
    btn.classList.remove(nameStyleActive);
  });
}

function setStyleFromStorage(valueStorageStyleName) {
  localStorage.setItem(nameKeyForStorage, valueStorageStyleName);
}

function getStyleFromStorage() {
  return localStorage.getItem(nameKeyForStorage);
}

function setStyle(objStyle) {
  let itemsCssName = Object.keys(objStyle);

  itemsCssName.forEach((nameCssClassInObj) => {
    let cssProperties = Object.entries(objStyle[nameCssClassInObj]);
    let items = document.querySelectorAll(nameCssClassInObj);

    if (items.length) {
      setCssPropertiesForItems(items, cssProperties);
    }
  });
}

function setCssPropertiesForItems(items, cssProperties) {
  items.forEach((item) => {
    cssProperties.forEach((cssStyle) => {
      item.style[cssStyle[0]] = cssStyle[1];
    });
  });
}

function clearStyle() {
  defaultStyle.forEach((nameCssClassInObj) => {
    let items = document.querySelectorAll(nameCssClassInObj);

    if (items.length) {
      items.forEach((item) => {
        item.removeAttribute('style');
      });
    }
  });
}